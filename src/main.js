// eslint-disable-next-line

import Vue from "vue";

import router from "./router";

import {store} from "./store";

const $ = require("jquery");
window.$ = $;

require("popper.js");

require("bootstrap");

require("bootstrap/dist/css/bootstrap.min.css");

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

require("./assets/main.css");

import axios from 'axios';

// Vue.use(axios);

import VueProgressBar from 'vue-progressbar';

const progressBarOptions = {
	color: '#3CFF33',
	failedColor: '#FF3333',
	thickness: '5px',
	transition: {
		speed: '0.2s',
		opacity: '0.6s',
		termination: 300
	},
	autoRevert: true,
	location: 'top',
	inverse: false
};
Vue.use(VueProgressBar, progressBarOptions);


import Swal from 'sweetalert2';
window.Swal = Swal;
const Toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 2000
});
window.Toast = Toast;

import ZoomOnHover from "vue-zoom-on-hover";
Vue.use(ZoomOnHover);


const REQUEST_TYPE_GET  = 'get';
const API_ROUTE = process.env.VUE_APP_API_URL + '/api/' + process.env.VUE_APP_API_VERSION + '/';
const ERROR_MESSAGE = 'Something went wrong. Please try again.';

Vue.prototype.$axiosCall = (route, data = {}) => {
	let url = API_ROUTE + route;
	return new Promise((resolve, reject) => {
		let thisVue = new Vue();
		thisVue.$Progress.start();
		let headers = {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		};
		axios({method: REQUEST_TYPE_GET, url: url, data: data, headers: headers})
				.then((successResponse) => {
					successResponse = successResponse.data;
					if (successResponse !== undefined && Object.prototype.hasOwnProperty.call(successResponse,'meta')) {
						if (successResponse.meta.status) {
							thisVue.$Progress.finish();
							resolve(successResponse);
						} else {
							Toast.fire({icon: 'error', title: successResponse.meta.message});
							thisVue.$Progress.fail();
							reject(successResponse);
						}
					} else {
						Toast.fire({icon: 'error', title: ERROR_MESSAGE});
						thisVue.$Progress.fail();
						reject(successResponse);
					}
				})
				.catch((error) => {
					thisVue.$Progress.fail();
					let errorResponse = error.response
							? error.response.data
									? error.response.data.message
											? error.response.data.message : ERROR_MESSAGE
									: ERROR_MESSAGE
							: ERROR_MESSAGE;
					Toast.fire({icon: 'error', title: errorResponse || ERROR_MESSAGE});
					reject(error);
				})
	});
};

Vue.config.productionTip = false;

import Layout from "./Layout.vue";

window.Fire = new Vue();

window.VARIANT_DEFAULT_PROPERTIES = ['_id', 'product_id', 'name', 'status', 'created_at', 'updated_at', 'product_id', 'media'];
window.VARIANT_DEFAULT_PRICE_PROPERTIES = ['price', 'discounted_price'];

Vue.filter('displayPropName', function (text) {
	return text.replace('_', ' ');
});

new Vue({
	router,
	store,
	render: h => h(Layout)
}).$mount("#app");
