import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import Home from "../views/Home.vue";
import Category from "../views/Category.vue";
import Product from "../views/Product.vue";
import Blog from "../views/Blog";

const routes = [
	{
		path: "/",
		name: "Home",
		component: Home
	},
	{
		path: "/categories/:categoryId/products",
		name: "Category",
		component: Category
	},
	{
		path: "/products/:productId",
		name: "Product",
		component: Product
	},
	{
		path: "/products/:productId/variants/:variantId",
		name: "Product",
		component: Product
	},
	{
		path: "/blogs/:blogId",
		name: "Blog",
		component: Blog
	},
];

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes
});

export default router;
