import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
	state: {
		categories: [],
		products: [],
		blogs: []
	},
	getters: {
		getCategories: (state) => {
			return state.categories;
		},
		getProducts: (state) => {
			return state.products;
		},
		getBlogs: (state) => {
			return state.blogs;
		}
	},
	mutations: {
		SET_CATEGORIES: (state, data) => {
			state.categories = data;
		},
		SET_PRODUCTS: (state, data) => {
			state.products = data;
		},
		SET_BLOGS: (state, data) => {
			state.blogs = data;
		}
	},
	actions: {
		setCategories: ({commit}, data) => {
			commit('SET_CATEGORIES', data);
		},
		setProducts: ({commit}, data) => {
			commit('SET_PRODUCTS', data);
		},
		setBlogs: ({commit}, data) => {
			commit('SET_BLOGS', data);
		}
	},
	modules: {}
});
